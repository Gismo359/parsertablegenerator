default: debug release

debug:
	$(CXX) -std=c++17 -g -o bin/debug/main.exe src/main.cpp

release:
	$(CXX) -std=c++17 -s -Ofast -march=native -flto -m64 -o bin/release/main.exe src/main.cpp
